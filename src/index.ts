import path from "path";

import fs from "fs-extra";
import cachedir from "cachedir";
import filenamify from "filenamify";

import { ChannelConnection } from "./connection";
import { Watcher } from "./watcher";
import { QualifiedAPI } from "./qualified";
import { File } from "./types";
import { diffInfo } from "./utils";
import { startSpinner, withSpinner } from "./ui/spinner";
import { prompt } from "./ui/prompt";
import { startAndWaitUntilExit } from "./ui/controller";

const PROJECT_FILE = ".qualified-attach.json";
const HISTORY_FILE = "history.json";
type History = { path: string; token: string };
const cacheDir = cachedir("qualified-attach");

export type Flags = { token?: string };

class QualifiedAttach {
  private resuming: boolean;
  private workdirName: string;
  private workdirPath: string;
  private files: File[];
  private watcher?: Watcher;
  private token: string;
  private conn?: ChannelConnection;
  private ablyToken: string;
  private channelName: string;
  private Q: QualifiedAPI;

  constructor() {
    this.resuming = false;
    this.workdirName = "";
    this.workdirPath = "";
    this.files = [];
    this.token = "";
    this.ablyToken = "";
    this.channelName = "";
    this.Q = new QualifiedAPI();
    // TODO clean up when interrupted (SIGINT)
  }

  async run(flags: Flags) {
    if (flags.token) this.token = flags.token;
    await this.maybeResume();

    if (this.token) {
      this.onToken(this.token);
    } else {
      this.showTokenPrompt();
    }
  }

  private async maybeResume() {
    // Allow resuming project if cwd contains project file and use the token from the file.
    if (await this.maybeResumeIn(process.cwd(), false)) return;
    if (!this.token) return;

    // Allow resuming from a known path if provided token matches the token and project file with same token exists.
    const historyFile = path.join(cacheDir, HISTORY_FILE);
    if (!(await fs.pathExists(historyFile))) return;

    const history: History[] = await fs.readJSON(historyFile);
    for (const h of history) {
      if (h.token === this.token && (await this.maybeResumeIn(h.path, true))) {
        break;
      }
    }
  }

  private async maybeResumeIn(wpath: string, check: boolean) {
    const projectFile = path.join(wpath, PROJECT_FILE);
    if (await fs.pathExists(projectFile)) {
      const o = await fs.readJSON(projectFile);
      if (check && this.token !== o.token) return false;

      this.token = o.token;
      this.workdirPath = wpath;
      this.workdirName = path.basename(wpath);
      this.resuming = true;
      return true;
    }
    return false;
  }

  private async appendToHistory() {
    const historyFile = path.join(cacheDir, HISTORY_FILE);
    let history: History[];
    if (await fs.pathExists(historyFile)) {
      history = await fs.readJSON(historyFile);
    } else {
      history = [];
    }
    history.push({ token: this.Q.token, path: this.workdirPath });
    await fs.outputJSON(historyFile, history);
  }

  private async startWatcher() {
    const watcher = new Watcher(this.workdirPath);
    await watcher.ready;
    return watcher;
  }

  private async connect() {
    const conn = new ChannelConnection({
      token: this.ablyToken,
      channelName: this.channelName,
      refreshToken: () => this.Q.refreshAblyToken(),
    });
    await conn.ready;
    return conn;
  }

  private async fetchChallenge() {
    // TODO clean this up
    try {
      const data = await withSpinner(
        "Fetching challenge information",
        this.Q.initAttach()
      );
      if (data.status === "pending") {
        this.handlePendingChallenge();
      } else {
        this.files = data.files;
        this.ablyToken = data.ablyToken;
        this.channelName = data.ablyChannel;
        if (!this.resuming) {
          this.workdirName = filenamify(data.challengeName).trim();
          this.showWorkdirNamePrompt();
        } else {
          await this.beforeResume();
          this.connectAndWatch();
        }
      }
    } catch (e) {
      if (e.response) {
        const res = e.response;
        if (res.data && res.data.reason) {
          console.error(res.data.reason);
        } else {
          console.error(res.statusText || "Internal Server Error");
        }
      } else {
        console.error(e.message);
      }
      process.exit(1);
    }
  }

  private async showTokenPrompt() {
    const token = await prompt({
      message: "challenge token:",
      validate(token) {
        if (token.trim() === "") return "Token cannot be empty";
        return true;
      },
      transform(token) {
        return token.trim();
      },
    });
    this.onToken(token);
  }

  private onToken(token: string): void {
    this.Q.token = token;
    this.fetchChallenge();
  }

  private async showWorkdirNamePrompt() {
    const name = await prompt({
      message: "working directory name:",
      defaultValue: this.workdirName,
      validate(name) {
        if (name === "") return "name cannot be empty";
        if (name !== filenamify(name)) {
          return "not a valid name for path";
        }
        return true;
      },
    });
    this.onWorkdirName(name);
  }

  private async onWorkdirName(name: string) {
    const workdirPath = path.join(process.cwd(), name);
    if (await fs.pathExists(workdirPath)) {
      console.log(`${workdirPath} already exists`);
      this.showWorkdirNamePrompt();
      return;
    }

    this.workdirName = name;
    this.workdirPath = workdirPath;
    this.initWorkspace();
  }

  private async beforeResume() {
    // Revert any changes to `.gitignore`/`.attachignore` when resuming
    const toRevert = this.files.filter(
      f => f.path === ".gitignore" || f.path === ".attachignore"
    );
    await Promise.all(toRevert.map(f => this.revertFile(f)));
  }

  private async revertFile(file: File) {
    const fpath = path.join(this.workdirPath, file.path);
    if (file.readonly) await fs.remove(fpath);
    await fs.outputFile(fpath, file.contents, {
      mode: file.readonly ? 0o444 : 0o666,
    });
  }

  private async initWorkspace() {
    const { stop } = startSpinner("Initializing workspace...");
    try {
      // Add project file to allow resuming without token and resuming from a known path with token.
      await fs.outputJSON(path.join(this.workdirPath, PROJECT_FILE), {
        token: this.Q.token,
      });
      for (const f of this.files) {
        if (f.directory) {
          await fs.mkdirs(path.join(this.workdirPath, f.path));
        } else {
          await fs.outputFile(path.join(this.workdirPath, f.path), f.contents, {
            mode: f.readonly ? 0o444 : 0o666,
          });
        }
      }
      stop();
    } catch (e) {
      stop();
      console.error(e.message);
      process.exit(1);
    }

    this.connectAndWatch();
  }

  private async connectAndWatch() {
    try {
      // Remember this token and path pair to allow resuming with just a token.
      if (!this.resuming) await this.appendToHistory();

      const conn = await withSpinner(
        "Connecting to Qualified...",
        this.connect()
      );
      this.conn = conn;

      const watcher = await withSpinner(
        "Starting file watcher...",
        this.startWatcher()
      );
      this.watcher = watcher;

      if (this.resuming) {
        const watched = await watcher.getWatchedFiles();
        await syncDifferences(this.files, watched, conn);
      }

      await startAndWaitUntilExit({ watcher, conn, resuming: this.resuming });
      watcher.close();
      conn.close();
    } catch (e) {
      if (this.watcher) this.watcher.close();
      if (this.conn) this.conn.close();
      console.error(e.message);
      process.exit(1);
    }
  }

  // TODO Consider giving an option to wait for event and pull information again
  private handlePendingChallenge(): void {
    console.log(`Make sure to start the challenge before attaching.`);
    process.exit(0);
  }
}

async function syncDifferences(
  fetched: File[],
  watched: File[],
  conn: ChannelConnection
) {
  const { added, deleted, updated } = diffInfo(fetched, watched);

  for (const path of Object.keys(deleted)) {
    conn.fileRemoved(path);
  }

  for (const [path, contents] of Object.entries(added)) {
    conn.fileAdded({ path, contents });
  }

  for (const [path, contents] of Object.entries(updated)) {
    conn.fileChanged({ path, contents });
  }
}

export default QualifiedAttach;

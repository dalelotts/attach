import { detailedDiff } from "deep-object-diff";

import { File, FileMap, FilesDiff } from "./types";

// Ignores directories
export function filesToMap(files: File[]): FileMap {
  return files.reduce(
    (o, { path, contents, directory }) =>
      directory ? o : ((o[path] = contents), o),
    {} as FileMap
  );
}

export function diffInfo(fetched: File[], watched: File[]): FilesDiff {
  return detailedDiff(filesToMap(fetched), filesToMap(watched)) as FilesDiff;
}

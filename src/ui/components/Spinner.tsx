// https://github.com/vadimdemedes/ink-spinner but handles win32 and uses hooks
import React, { useState } from "react";
import { Box, Color } from "ink";
import spinners, { SpinnerName } from "cli-spinners";

import useInterval from "../hooks/useInterval";

export type SpinnerProps = {
  type?: SpinnerName;
  message: string;
};

const defaultSpinner: SpinnerName =
  process.platform === "win32" ? "line" : "dots";

const Spinner = ({ type = defaultSpinner, message }: SpinnerProps) => {
  const [frame, setFrame] = useState(0);
  const { frames, interval } = spinners[type] || spinners.dots;
  useInterval(() => {
    setFrame((frame + 1) % frames.length);
  }, interval);

  return (
    <Box>
      <Color green>{frames[frame]}</Color> {message}
    </Box>
  );
};

export default Spinner;

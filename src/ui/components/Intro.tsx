import React from "react";
import { Box, Color, Text } from "ink";

type Props = {
  workdirPath: string;
  resuming: boolean;
};

function Intro({ workdirPath, resuming }: Props) {
  return (
    <Box flexDirection="column" width="100%" paddingX={2} marginY={1}>
      <Box textWrap="wrap">
        {resuming ? "Resuming with" : "Initialized"} workspace under{" "}
        <Color green>{workdirPath}</Color>
      </Box>

      <Box marginTop={1} textWrap="wrap">
        You are now ready to take the challenge using your editor.
      </Box>
      <Box textWrap="wrap">
        Simply navigate your editor to the path displayed above and start
        writing code.
      </Box>

      <Box marginTop={1} textWrap="wrap">
        If enabled, we will rerun the tests every time you save a file and the
        results will be displayed in the browser.
      </Box>

      <Box textWrap="wrap">
        You can also trigger to run tests from this terminal by pressing{" "}
        <Text bold>t</Text>.{"\n"}
        Once you're finished, you can submit your solution by pressing{" "}
        <Text bold>s</Text>.
      </Box>
    </Box>
  );
}

export default Intro;

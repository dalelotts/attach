import React from "react";
import { render } from "ink";

import AttachController, { AttachControllerProps } from "./AttachController";

export async function startAndWaitUntilExit({
  conn,
  watcher,
  resuming,
}: AttachControllerProps) {
  const { waitUntilExit } = render(
    <AttachController conn={conn} watcher={watcher} resuming={resuming} />
  );
  await waitUntilExit();
}

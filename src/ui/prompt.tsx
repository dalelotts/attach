import React from "react";
import { render } from "ink";

import Prompt from "./components/Prompt";

export type PromptOptions = {
  message: string;
  defaultValue?: string;
  validate?: (v: string) => true | string;
  transform?: (v: string) => string;
};

export async function prompt({
  message,
  defaultValue,
  validate,
  transform,
}: PromptOptions): Promise<string> {
  const [value, instance] = await new Promise(resolve => {
    const instance = render(
      <Prompt
        message={message}
        placeholder={defaultValue}
        validate={validate}
        transform={transform}
        onSubmit={v => {
          resolve([v, instance]);
        }}
      />
    );
  });
  instance.unmount();
  await instance.waitUntilExit();

  return value;
}

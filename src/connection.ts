import Ably from "ably/promises";
import { Types } from "ably";
import { File, AlertType } from "./types";

export interface ChannelConnectionOptions {
  token: string;
  channelName: string;
  refreshToken: () => Promise<string>;
}

type PresenceCallback = (message: Types.PresenceMessage) => void;

export class ChannelConnection {
  client: Ably.Realtime;
  channel: Types.RealtimeChannelPromise;
  clientId: string;
  ready: Promise<boolean>;

  constructor({ token, channelName, refreshToken }: ChannelConnectionOptions) {
    // Token contains `clientId` so we don't need to specify.
    this.client = new Ably.Realtime({
      token,
      authCallback: (_tokenParams, callback) => {
        refreshToken()
          .then(t => callback("", t))
          .catch(e => callback(e, ""));
      },
    });
    // Channel name looks like attach:candidate_id:challenge_id
    this.clientId = channelName.split(":")[1];
    this.channel = this.client.channels.get(channelName);
    this.ready = this.connect();
  }

  private async connect() {
    const members = await this.channel.presence.get({
      clientId: this.clientId,
    });
    if (members.find(m => m.data && m.data.app === "attach")) {
      throw new Error(
        "Another instance of qualified-attach is already present"
      );
    }

    const assess = members.find(m => m.data && m.data.app === "assess");
    if (!assess) await this.waitForAssess();
    // TODO Handle browser disconnection
    // this.channel.presence.subscribe('leave', onLeave);

    // `presence.enter` will attach to channel implicitly
    // The browser should always ensure that connection with
    // `clientId: candidate_id` and `app: "attach"` is present on channel.
    // Presence will emit `leave` even when Attach disconnects unexpectedly.
    await this.channel.presence.enter({ app: "attach" });
    return true;
  }

  async close() {
    await this.client.close();
  }

  private waitForAssess() {
    return new Promise(resolve => {
      const onEnter: PresenceCallback = ({ clientId, data }) => {
        if (clientId === this.clientId && data && data.app === "assess") {
          this.channel.presence.unsubscribe("enter", onEnter);
          resolve();
        }
      };
      // Note that subscribing attaches to the channel implicitly.
      this.channel.presence.subscribe("enter", onEnter);
    });
  }

  // Shows an alert within the web IDE
  alert(message: string, type: AlertType = "info") {
    this.channel.publish("show_alert", { message, type });
  }

  fileAdded(file: File): void {
    this.channel.publish("file_added", file);
  }

  fileChanged(file: File): void {
    this.channel.publish("file_changed", file);
  }

  fileRemoved(file: string): void {
    this.channel.publish("file_removed", { path: file });
  }

  test(): void {
    this.channel.publish("test", {});
  }

  submit(): void {
    this.channel.publish("submit", {});
  }
}

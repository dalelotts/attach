## qualified-attach

[![Version](https://img.shields.io/npm/v/@qualified/attach.svg)](https://npmjs.org/package/@qualified/attach)
[![Downloads/week](https://img.shields.io/npm/dw/@qualified/attach.svg)](https://npmjs.org/package/@qualified/attach)
[![License](https://img.shields.io/npm/l/@qualified/attach.svg)](https://gitlab.com/qualified/attach/blob/master/package.json)

<!-- toc -->

- [Usage](#usage)
  <!-- tocstop -->

# Usage

<!-- usage -->

```sh-session
$ npm install -g @qualified/attach

$ qualified-attach
set up local workspace...

$ qualified-attach --help
Attach local files to Qualified challenge

USAGE
  $ qualified-attach

OPTIONS
  -h, --help         show help
  -t, --token=token  token provided by web IDE
  -v, --version      show version
...
```

<!-- usagestop -->

import Ably from "ably";
import nock from "nock";

import { AttachInitResponse } from "../src/types";

const ABLY_KEY = process.env.ABLY_KEY || "";
const CANDIDATE_ID = "1";
const CHALLENGE_ID = "1";

export function stubApis(baseURL: string) {
  nock(baseURL)
    .persist()
    .post("/attach")
    .delay(1000)
    .reply(200, function(_path, _requestBody, f) {
      fakeInit(this.req.headers.authorization).then(r => f(null, r));
    })
    .post("/attach/ably_token")
    .reply(200, function(_path, _requestBody, f) {
      localAblyToken().then(r => f(null, r));
    });
}

type AblyTokenResponse = { token: string };

async function localAblyToken(): Promise<AblyTokenResponse> {
  return { token: await getAblyToken() };
}

async function fakeInit(token: string): Promise<AttachInitResponse> {
  const status = token.startsWith("p") ? "pending" : "started";

  return {
    challengeName: "example",
    challengeId: CHALLENGE_ID,
    candidateId: CANDIDATE_ID,
    ablyToken: await getAblyToken(),
    ablyChannel: `attach:${CANDIDATE_ID}:${CHALLENGE_ID}`,
    status,
    files:
      status === "pending"
        ? []
        : [
            {
              path: "src/solution.js",
              contents: "module.exports = (a, b) => a + b;",
            },
            {
              path: "src/foo.js",
              contents: "module.exports = (a, b) => a + b;",
            },
            {
              path: "src/bar.js",
              contents: "module.exports = (a, b) => a + b;",
            },
            {
              path: "test/solution.spec.js",
              contents: [
                `const { expect } = require("chai");`,
                `const add = require("../src/solution");`,
                `describe("add", () => {`,
                `  it("returns sum", () => {`,
                `    expect(add(1, 1)).to.equal(2);`,
                `  });`,
                `});`,
              ].join("\n"),
            },
          ],
  };
}

async function getAblyToken(): Promise<string> {
  if (!ABLY_KEY) throw new Error(`ABLY_KEY is not defined`);
  const client = new Ably.Rest.Promise(ABLY_KEY);
  const details = await client.auth.requestToken({
    clientId: CANDIDATE_ID,
    capability: {
      [`attach:${CANDIDATE_ID}:*`]: ["publish", "subscribe", "presence"],
    },
    // Give short ttl to test `authCallback`
    ttl: 30 * 1000,
  });
  return details.token;
}

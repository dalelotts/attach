declare module "ink-divider" {
  interface DividerProps {
    title?: string;
    width?: number;
    padding?: number;
    titlePadding?: number;
    titleColor?: string;
    dividerChar?: string;
    dividerColor?: string;
  }

  function Divider(props: DividerProps): JSX.Element;

  export = Divider;
}

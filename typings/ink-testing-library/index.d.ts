declare module "ink-testing-library" {
  import React from "react";

  export interface TestStdin {
    write(frame: string): void;
  }

  export interface TestInstance {
    rerender: <Props>(tree: React.ReactElement<Props>) => void;
    unmount: () => void;
    stdin: TestStdin;
    frames: string[];
    lastFrame?: string;
  }

  export function render<Props>(tree: React.ReactElement<Props>): TestInstance;
  export function cleanup(): void;
}

declare module "ink-tab" {
  import React from "react";

  export interface TabProps {
    name: string;
    children: React.ReactNode;
  }
  export class Tab extends React.Component<TabProps> {}

  export interface TabsKeyMap {
    useNumbers: boolean;
    useTab: boolean;
    previous: string[];
    next: string[];
  }
  export interface TabsProps {
    onChange: (name: string, tab: Tab) => void;
    children: React.ReactNode;
    flexDirection?: "row" | "row-reverse" | "column" | "column-reverse";
    keyMap?: TabsKeyMap | null;
  }
  export interface TabsState {
    activeTab: number;
  }
  export class Tabs extends React.Component<TabsProps, TabsState> {}
}

declare module "meow" {
  import { PackageJson } from "type-fest";
  import { Options as MinimistOptions } from "minimist-options";

  namespace meow {
    interface Options {
      readonly flags?: MinimistOptions;
      readonly description?: string | false;
      readonly help?: string | false;
      readonly version?: string | false;
      readonly autoHelp?: boolean;
      readonly autoVersion?: boolean;
      readonly pkg?: { [key: string]: unknown };
      readonly argv?: ReadonlyArray<string>;
      readonly inferType?: boolean;
      readonly booleanDefault?: boolean | null | undefined;
      readonly hardRejection?: boolean;
    }

    interface Result {
      input: string[];
      flags: { [name: string]: unknown };
      unnormalizedFlags: { [name: string]: unknown };
      pkg: PackageJson;
      help: string;
      showHelp(exitCode?: number): void;
      showVersion(): void;
    }
  }

  function meow(helpMessage: string, options?: meow.Options): meow.Result;
  function meow(options?: meow.Options): meow.Result;

  export = meow;
}
